<link rel="stylesheet" type="text/css" href="css/badge.css">
<script type="text/javascript" language="javascript" src="jquery-1.12.3.js"></script>
<script type="text/javascript" charset="utf-8">//<![CDATA[
$(document).ready(function() {
//BACKGROUND==========================================
var newClass = function(div, next) {
    div[0].className = div[0].className.replace(/badge-bg\d+/, "badge-bg" + newClass[next ? 'next' : 'prev']());
};
newClass.atual = 1;
newClass.last = 13;
newClass.next = function(){
    this.atual = (this.atual == this.last ? 1 : this.atual + 1);
    return this.atual;
};
newClass.prev = function(){
    this.atual = (this.atual == 1 ? this.last : this.atual - 1);
    return this.atual;
};


var myDiv = $("#b-bg");
$(".btnN").click(function () { 
    newClass(myDiv, true);
});
$(".btnP").click(function () { 
    newClass(myDiv, false);
});

//PATTERN==========================================
var newClass2 = function(div, next) {
    div[0].className = div[0].className.replace(/badge-p\d+/, "badge-p" + newClass2[next ? 'next' : 'prev']());
};
newClass2.atual = 1;
newClass2.last = 79;
newClass2.next = function(){
    this.atual = (this.atual == this.last ? 1 : this.atual + 1);
    return this.atual;
};
newClass2.prev = function(){
    this.atual = (this.atual == 1 ? this.last : this.atual - 1);
    return this.atual;
};


var myDiv2 = $("#b-p");
$(".btnN2").click(function () { 
    newClass2(myDiv2, true);
});
$(".btnP2").click(function () { 
    newClass2(myDiv2, false);
});


//BORDER==========================================
var newClass3 = function(div, next) {
    div[0].className = div[0].className.replace(/badge-b\d+/, "badge-b" + newClass3[next ? 'next' : 'prev']());
};
newClass3.atual = 1;
newClass3.last = 20;
newClass3.next = function(){
    this.atual = (this.atual == this.last ? 1 : this.atual + 1);
    return this.atual;
};
newClass3.prev = function(){
    this.atual = (this.atual == 1 ? this.last : this.atual - 1);
    return this.atual;
};


var myDiv3 = $("#b-b");
$(".btnN3").click(function () { 
    newClass3(myDiv3, true);
});
$(".btnP3").click(function () { 
    newClass3(myDiv3, false);
});

//LEVEL==========================================
var newClass4 = function(div, next) {
    div[0].className = div[0].className.replace(/badge-lvl\d+/, "badge-lvl" + newClass4[next ? 'next' : 'prev']());
	$("#p-lvl").html('<span id="p-lvl" class="badge-l">' + newClass4.atual + '</span>');
};
newClass4.atual = 1;
newClass4.last = 10;
newClass4.next = function(){
    this.atual = (this.atual == this.last ? 1 : this.atual + 1);
    return this.atual;
};
newClass4.prev = function(){
    this.atual = (this.atual == 1 ? this.last : this.atual - 1);
    return this.atual;
};


var myDiv4 = $("#b-lvl");
$(".btnN4").click(function () { 
    newClass4(myDiv4, true);
});
$(".btnP4").click(function () { 
    newClass4(myDiv4, false);
});

});
//]]></script>
<?php

$json3 = '1';
$borderc = '1';
$backroundc = '1';
$patternc = '1';

echo'
<span class="badge-large">
<span id="b-b" class="badge-b badge-b'.$borderc.'"></span>
<span class="badge">
<span id="b-bg" class="badge-bg badge-bg'.$backroundc.'"></span>
<span id="b-p" class="badge-p badge-p'.$patternc.'"></span>
<span class="badge-r"></span>
<span id="p-lvl" class="badge-l"><span>'.$json3.'</span></span>
</span>
<span id="b-lvl" class="badge-lvl badge-lvl'.$json3.'"></span>
</span>';
?>




<input type="button" class="btnP" value="Prev"/>
<input type="button" class="btnN" value="Next"/>
<br/>

<input type="button" class="btnP2" value="Prev2"/>
<input type="button" class="btnN2" value="Next2"/>
<br/>

<input type="button" class="btnP3" value="Prev3"/>
<input type="button" class="btnN3" value="Next3"/>

<br/>

<input type="button" class="btnP4" value="Prev4"/>
<input type="button" class="btnN4" value="Next4"/>